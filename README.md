Final Fantasy XIV: A Realm Reborn Macros
============  
By: Aiel Waste  
Server: Sargatanas  
Free Company: [OMG!](http://omg.guildwork.com)  
  
A place to list some of your macros, explain how they work, and why you use them.  

Here is the [wiki](https://github.com/andrewbell8/ffxiv_macros/wiki) with documentation.  
